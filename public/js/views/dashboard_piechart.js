$(function () {
    'use strict';
    var ctx1 = document.getElementById("pie1");
    var ctx2 = document.getElementById("pie2");
    var ctx3 = document.getElementById("pie3");
    var ctx4 = document.getElementById("pie4");
    var ctx5 = document.getElementById("pie5");
    var ctx6 = document.getElementById("pie6");
    var ctx7 = document.getElementById("pie7");
    var ctx8 = document.getElementById("pie8");

    var myChart1 = new Chart(ctx1, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [10, 20],
                    backgroundColor: [
                        'blue',
                        'yellow',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Senarai Tempat Kediaman Beramai-ramai'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart2 = new Chart(ctx2, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [10, 20],
                    backgroundColor: [
                        'blue',
                        'yellow',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Senarai Tempat Kediaman Beramai-ramai'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart3 = new Chart(ctx3, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [10, 20],
                    backgroundColor: [
                        'blue',
                        'yellow',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Senarai Tempat Kediaman Beramai-ramai'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart4 = new Chart(ctx4, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [10, 20],
                    backgroundColor: [
                        'blue',
                        'yellow',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Senarai Tempat Kediaman Beramai-ramai'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart5 = new Chart(ctx5, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [10, 20],
                    backgroundColor: [
                        'blue',
                        'yellow',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Senarai Tempat Kediaman Beramai-ramai'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
    
     var myChart6 = new Chart(ctx6, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [10, 20],
                    backgroundColor: [
                        'blue',
                        'yellow',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Senarai Tempat Kediaman Beramai-ramai'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart7 = new Chart(ctx7, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [10, 20],
                    backgroundColor: [
                        'blue',
                        'yellow',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Senarai Tempat Kediaman Beramai-ramai'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart8 = new Chart(ctx8, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [10, 20],
                    backgroundColor: [
                        'blue',
                        'yellow',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Senarai Tempat Kediaman Beramai-ramai'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
});
