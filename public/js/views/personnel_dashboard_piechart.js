$(function () {
    'use strict';
    var canvas1 = document.getElementById("pie_pembanci");
    var canvas2 = document.getElementById("pie_penyelia");
    var ctx1 = document.getElementById("pie_pembanci").getContext('2d');
    var ctx2 = document.getElementById("pie_penyelia").getContext('2d');

    var myChart1 = new Chart(ctx1, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [10, 20],
                    backgroundColor: [
                        'lightblue',
                        'orange',
                    ]
                }, ],

            labels: ["Jumlah Kekosongan Pembanci", "Jumlah Pembanci Yang Telah Disi"]
        },
        options: {
            title: {
                display: true,
                text: 'Peratusan Jawatan Pembanci Di Malaysia'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 9,
                    usePointStyle: true
                },

            }
        }
    });

    var myChart2 = new Chart(ctx2, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [10, 20],
                    backgroundColor: [
                        'darkblue',
                        'red',
                    ]
                }, ],

            labels: ["Jumlah Penyelia Yang Telah Disi", "Jumlah Penyelia Yang Diperlukan"]
        },
        options: {
            title: {
                display: true,
                text: 'Peratusan Jawatan Penyelia Di Malaysia'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 9,
                    usePointStyle: true
                },

            }
        }
    });

});
