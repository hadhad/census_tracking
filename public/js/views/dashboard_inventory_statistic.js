$(function () {
    'use strict';
    var ctx2 = document.getElementById("inventory_statistic_by_state").getContext('2d');
    var myChart2 = new Chart(ctx2, {
        type: 'bar',
        data: {
            labels: ["MALAYSIA", "Johor", "Kedah", "Kelantan", "Melaka", "Negeri Sembilan", "Pahang", "Pulau Pinang"
                        , "Perak", "Perlis", "Selangor", "Terengganu", "Sabah", "Sarawak", "W.P Kuala Lumpur", "W.P Labuan", "W.P Putrajaya"],
            datasets:
                    [{
                            label: 'Selesai',
                            data: [41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22],
                            backgroundColor: 'blue'
                        },
                        {
                            label: 'Belum Selesai',
                            data: [41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22],
                            backgroundColor: 'yellow'
                        },
                    ]
        },
        options: {
            legend: {
                display: true,
                position: 'bottom',
            },
            title: {
                display: true,
                text: 'Senarai Penghantaran/Penerimaan Inventori Mengikut Negeri'
            },
            scales: {
                xAxes: [{
                        ticks: {
                            autoSkip: false,
                            fontSize: 9,
                        },
                        stacked: true,
                    }],
                yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            steps: 10,
                            stepValue: 5,
                            max: 100
                        },
                        stacked: true
                    }]
            },
        }
    });
});
