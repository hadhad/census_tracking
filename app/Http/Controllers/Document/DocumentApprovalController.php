<?php

namespace App\Http\Controllers\Document;

class DocumentApprovalController extends \App\Http\Controllers\Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('document.document_approval');
	}

	/**
	 * Create the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('document.document_approval_create');
	}

	/**
	 * Edit the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit() {
		return view('document.document_approval_edit');
	}
}
