<?php

namespace App\Http\Controllers\Inventory;

class InventoryDeliveryPreCensusController extends \App\Http\Controllers\Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function create() {
		return view('inventory.inventory_delivery_pre_census_create');
	}
}
