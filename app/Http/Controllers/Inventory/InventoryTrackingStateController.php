<?php

namespace App\Http\Controllers\Inventory;

class InventoryTrackingStateController extends \App\Http\Controllers\Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('inventory.inventory_tracking_state');
	}

	/**
	 * Create the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('inventory.inventory_tracking_state_create');
	}

	/**
	 * Edit the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit() {
		return view('inventory.inventory_tracking_state_edit');
	}
}
