<?php

namespace App\Http\Controllers\Personnel;

class PersonnelRecruitmentListController extends \App\Http\Controllers\Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('personnel.recruitment_list');
	}
        
	public function edit() {
		return view('personnel.personnel_edit');
	}
}
