<?php

namespace App\Http\Controllers\Asset;

class AssetDeliveryPostCensusController extends \App\Http\Controllers\Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('asset.asset_delivery_post_census_create');
	}
}
