<?php

namespace App\Http\Controllers\Vendor;

class VendorDashboardController extends \App\Http\Controllers\Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('vendor.vendor_dashboard');
	}

	/**
	 * Create the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function list() {
		return view('vendor.vendor_list');
	}

	/**
	 * Edit the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit() {
		return view('vendor.vendor_management');
	}
}
