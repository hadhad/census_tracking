@extends('master')
@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="col-lg-4">
            <div class="col-lg-12">
                <div class="row">
                    <center>Maklumat Baki Dokumen Setiap Negeri</center>
                </div>
            </div>
        </div>
        <br>
        <div class="row">

            <div class="col-lg-2">
                <div class="card">
                    <div class="card-body">JOHOR</div>
                </div>
                <div class="card">
                    <div class="card-body">5000<br/>Jumlah Personel</div>
                </div>
                <div class="card">
                    <div class="card-body">3000<br/>Jumlah Personel Lelaki</div>
                </div>
                <div class="card">
                    <div class="card-body">2000<br/>Jumlah Personel Perempuan</div>
                </div>
                <div class="card">
                    <div class="card-body">5000<br/>Jumlah Personel Diwartakan</div>
                </div>
                <div class="card">
                    <div class="card-body">5000<br/>Jumlah Personel Tidak Diwartakan</div>
                </div>

            </div>
            <!-- /.col-->
            <div class="col-lg-10">
                <div class="form-group row">
                    <div class="col-md-6 text-center">
                        <select class="form-control form-control-sm" id="select3" name="select3">
                            <option value="0">Jawatan</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                        </select>
                    </div>
                    <div class="col-sm-6 text-center">
                        <select class="form-control form-control-sm" id="select3" name="select3">
                            <option value="0">Kumpulan</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                        </select>
                        <br/>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <table class="table table-responsive-sm table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Nombor K/P</th>
                                    <th>No Telefon</th>
                                    <th>Jawatan</th>
                                    <th>Kumpulan</th>
                                    <th>Email</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Ahmad Faiz</td>
                                    <td>902919-01-3391</td>
                                    <td>0129993334</td>
                                    <td>Pembanci</td>
                                    <td>Bukan Warta</td>
                                    <td>faiz@gmail.com</td>
                                    <td>
                                      <!-- <span class="badge badge-success">Active</span> -->
                                        <a href="{{ route('personnel.edit',1) }}" class="btn btn-sm btn-success" type="submit">
                                            <i class="fa fa-dot-circle-o"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="#">Prev</a>
                            </li>
                            <li class="page-item active">
                                <a class="page-link" href="#">1</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">2</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">3</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">4</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
        @endsection
        <!-- /.conainer-fluid -->

