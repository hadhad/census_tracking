@extends('master')
@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <!-- /.col-->
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Maklumat Peribadi</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12"  >
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Nama</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">No K/P</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Jantina</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Status Perkahwinan</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Bangsa</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Agama</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Umur</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Negeri</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">No Tel</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Status</label>
                                    <div class="col-md-4">
                                        <select class="form-control form-control-sm" id="select3" name="select3">
                                            <option value="0">Penggantian</option>
                                            <option value="1">Option #1</option>
                                            <option value="2">Option #2</option>
                                            <option value="3">Option #3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Email</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Penggantian</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12"  >
                                <div class="row">
                                    <div class="col-md-4">
                                        <select class="form-control form-control-sm" id="select3" name="select3">
                                            <option value="0">Negeri</option>
                                            <option value="1">Option #1</option>
                                            <option value="2">Option #2</option>
                                            <option value="3">Option #3</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select class="form-control form-control-sm" id="select3" name="select3">
                                            <option value="0">Daerah</option>
                                            <option value="1">Option #1</option>
                                            <option value="2">Option #2</option>
                                            <option value="3">Option #3</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select class="form-control form-control-sm" id="select3" name="select3">
                                            <option value="0">Simpanan</option>
                                            <option value="1">Option #1</option>
                                            <option value="2">Option #2</option>
                                            <option value="3">Option #3</option>
                                        </select>
                                    </div>
                                </div>
                                <br/>
                                <table class="table table-responsive-sm table-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>No K/P</th>
                                            <th>No Telefon</th>
                                            <th>Email</th>
                                            <th>Jawatan Dipohon</th>
                                            <th>Ganti</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td><a href="{{ route('recruitment_info.index') }}">Abu</a></td>
                                            <td>821923-01-9920</td>
                                            <td>0129929233</td>
                                            <td>abu@gmail.com</td>
                                            <td>Pembanci</td>
                                            <td class="text-center"><input type="checkbox" /></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>  
                        </div>
                    </div>

                    <center>
                        <button class="btn btn-success" type="submit">Kemaskini</button>
                        <button class="btn btn-primary" type="submit">Hantar</button>
                    </center>
                    <br/>
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
    @endsection
    <!-- /.conainer-fluid -->

