@extends('master')
@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-2">
                <div class="card">
                    <div class="card-body">JOHOR</div>
                </div>
                <div class="card">
                    <div class="card-body">5000<br/>Jumlah Calon Memohon</div>
                </div>
                <div class="card">
                    <div class="card-body">3000<br/>Jumlah Lelaki</div>
                </div>
                <div class="card">
                    <div class="card-body">2000<br/>Jumlah Perempuan</div>
                </div>
                <div class="card">
                    <div class="card-body">5000<br/>Jumlah Pemohon Jawatan Penyelia</div>
                </div>
                <div class="card">
                    <div class="card-body">5000<br/>Jumlah Personel Jawatan Pembanci</div>
                </div>

            </div>
            <!-- /.col-->
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-md-8">
                        &nbsp;
                    </div>
                    <div class="col-md-4 input-group">
                        <input class="form-control" id="input1-group2" type="text" name="input1-group2" placeholder="Carian">
                        <span class="input-group-prepend">
                            <button class="btn btn-primary" type="button">
                                <i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </div>
                <br/>
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Pengambilan Personel
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ route('personnel_recruitment.index') }}">Senarai Calon</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('recruitment_interview_invitation.index') }}">Jemputan Temuduga</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('recruitment_interview_result.index') }}">Keputusan Temuduga</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('recruitment_acceptance.index') }}">Calon Bersetuju</a>
                            </li>
                        </ul>
                        <br/>
                        <div class="row">
                            <div class="col-md-2">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0">Negeri</option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0">Daerah Pentadbiran</option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0">Daerah Banci</option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0">Pembanci</option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                               &nbsp;
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-block btn-primary">Cari</button>
                            </div>
                        </div>
                        <br/>
                        <table class="table table-responsive-sm table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>No K/P</th>
                                    <th>No Telefon</th>
                                    <th>Email</th>
                                    <th>Jawatan Dipohon</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td><a href="{{ route('recruitment_info.index') }}">Abu</a></td>
                                    <td>821923-01-9920</td>
                                    <td>0129929233</td>
                                    <td>abu@gmail.com</td>
                                    <td>Pembanci</td>
                                    <td>Calon</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
        @endsection
        <!-- /.conainer-fluid -->

