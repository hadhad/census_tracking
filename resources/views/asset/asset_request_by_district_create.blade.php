@extends('master')
@section('content')

<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <strong>Credit Card</strong>
            <small>Form</small>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control" id="name" type="text" placeholder="Enter your name">
                    </div>
                </div>
            </div>
            <!-- /.row-->
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="ccnumber">Credit Card Number</label>
                        <input class="form-control" id="ccnumber" type="text" placeholder="0000 0000 0000 0000">
                    </div>
                </div>
            </div>
            <!-- /.row-->
            <div class="row">
                <div class="form-group col-sm-4">
                    <label for="ccmonth">Month</label>
                    <select class="form-control" id="ccmonth">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                        <option>10</option>
                        <option>11</option>
                        <option>12</option>
                    </select>
                </div>
                <div class="form-group col-sm-4">
                    <label for="ccyear">Year</label>
                    <select class="form-control" id="ccyear">
                        <option>2014</option>
                        <option>2015</option>
                        <option>2016</option>
                        <option>2017</option>
                        <option>2018</option>
                        <option>2019</option>
                        <option>2020</option>
                        <option>2021</option>
                        <option>2022</option>
                        <option>2023</option>
                        <option>2024</option>
                        <option>2025</option>
                    </select>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="cvv">CVV/CVC</label>
                        <input class="form-control" id="cvv" type="text" placeholder="123">
                    </div>
                </div>
            </div>
            <!-- /.row-->
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Permintaan Aset Bagi Daerah</div>
        <div class="card-body">
            <a href="{{route('asset_request_district.create')}}" class="btn btn-primary">Permintaan</a>
            <table class="table table-sm table-responsive-lg table-responsive-sm">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tarikh Permintaan</th>
                        <th>Kod Aset</th>
                        <th>Nama Aset</th>
                        <th>Daerah Pemohon</th>
                        <th>Daerah Dipohon</th>
                        <th>Kuantiti</th>
                        <th>Status</th>
                        <th>Tindakan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>30/09/2018</td>
                        <td>2</td>
                        <td>Soal Selidik</td>
                        <td>Johor Bahru</td>
                        <td>Alor Gajah</td>
                        <td>100</td>
                        <td>
                            <span class="badge badge-info">Lulus</span>
                        </td>
                        <td>
                            <i class="icon-pencil" />
                            <i class="icon-trash" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="#">Prev</a>
                </li>
                <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">2</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">3</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">4</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </div>
    </div>
</div>
@endsection
<!-- /.conainer-fluid -->

