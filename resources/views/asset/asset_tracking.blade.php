@extends('master')
@section('content')

<div class="container-fluid">

    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <label class="col-md-1 col-form-label" for="select3">Fasa Pembancian</label>
                    <div class="col-md-3">
                      <select class="form-control form-control-sm" id="select3" name="select3">
                        <option value="0">Pra Banci</option>
                        <option value="1">Option #1</option>
                        <option value="2">Option #2</option>
                        <option value="3">Option #3</option>
                      </select>
                    </div>
                    <label class="col-md-1 col-form-label" for="select3">Jenis Aset</label>
                    <div class="col-md-3">
                      <select class="form-control form-control-sm" id="select3" name="select3">
                        <option value="0">Keseluruhan</option>
                        <option value="1">Option #1</option>
                        <option value="2">Option #2</option>
                        <option value="3">Option #3</option>
                      </select>
                    </div>
                    <label class="col-md-1 col-form-label" for="select3">Peringkat Pegawai</label>
                    <div class="col-md-3">
                      <select class="form-control form-control-sm" id="select3" name="select3">
                        <option value="0">Timbalan Pesuruhjaya</option>
                        <option value="1">Option #1</option>
                        <option value="2">Option #2</option>
                        <option value="3">Option #3</option>
                      </select>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie1" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie2" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie3" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie4" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie5" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie6" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie7" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie8" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie9" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie10" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie11" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie12" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie13" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie14" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie15" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie16" ></canvas>
                            </div>
                        </div>
                    </div>
<!--                 </div>
                <div class="row">
                    <canvas id="inventory_statistic_by_state" height="140"></canvas>
                </div> -->
                <ul class="pagination">
                  <li class="page-item">
                    <a class="page-link" href="#">Sebelum</a>
                  </li>
                  <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="#">2</a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="#">3</a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="#">4</a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="#">Seterusnya</a>
                  </li>
                </ul>
            </div>
<!--             <div class="col-sm-4">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="text-center card-body pb-0">
                                <h3>10,000,000</h3>
                                Aset
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="text-center card-body pb-0">
                                <h3>5,000</h3>
                                Aset
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="text-center card-body pb-0">
                                <h3>10,000</h3>
                                Inventori
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="text-center card-body pb-0">
                                <h3>3,500</h3>
                                Personel
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="text-center card-body pb-0">
                                <h3>90</h3>
                                Vendor
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="text-center card-body pb-0">
                                <h3>50</h3>
                                Premis
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <canvas id="personel_stats" height="400"></canvas>
                </div>
            </div> -->
            <!--/.col-->
        </div>
        <!--/.row-->
    </div>

</div>
@endsection
<!-- /.conainer-fluid -->

@section('myscript')
<!-- <script src="{{ asset('js/views/dashboard_dokumen_statistic.js') }}"></script>
<script src="{{ asset('js/views/dashboard_inventory_statistic.js') }}"></script> -->
<script src="{{ asset('js/views/asset_tracking_piechart.js') }}"></script>
<!-- <script src="{{ asset('js/views/dashboard_personel.js') }}"></script> -->
@endsection