@extends('master')
@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <!-- /.col-->
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Pengurusan Rekod Stor</div>
                    <div class="card-body">
                        <div class="text-center">
                            REKOD PENGHANTARAN/PENERIMAAN ASET PERINGKAT STOR
                        </div>
                        <br/>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="select3">Nama Penghantar</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                            </div>
                            <label class="col-md-2 col-form-label" for="select3">Jawatan</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="select3">Nama Pembekal</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                            </div>
                            <label class="col-md-2 col-form-label" for="select3">Lokasi Stor</label>
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0"></option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="select3">Negeri</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                            </div>
                            <label class="col-md-2 col-form-label" for="select3">Kuota</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="select3">No Kod</label>
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0"></option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                            <label class="col-md-2 col-form-label" for="select3">Jenis Aset</label>
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0"></option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="select3">No. DO</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                            </div>
                            <label class="col-md-2 col-form-label" for="select3">Mod Penghantaran</label>
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0"></option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="select3">Kuantiti Hantar</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="number" name="input1-group2" placeholder="">
                            </div>
                            <label class="col-md-2 col-form-label" for="select3">Tarikh Hantar</label>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <input class="form-control form-control-sm" id="input1-group2" type="date" name="input1-group2" placeholder="Cari Nama Aset">
                                    <span class="input-group-prepend">
                                        <button class="btn btn-secondary" type="button">
                                            <i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <hr/>
                        <br/>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="select3">Nama Penerima</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                            </div>
                            <label class="col-md-2 col-form-label" for="select3">Jawatan</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="select3">Kuantiti Terima</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="number" name="input1-group2" placeholder="">
                            </div>
                            <label class="col-md-2 col-form-label" for="select3">Tarikh Terima</label>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <input class="form-control form-control-sm" id="input1-group2" type="date" name="input1-group2" placeholder="Cari Nama Aset">
                                    <span class="input-group-prepend">
                                        <button class="btn btn-secondary" type="button">
                                            <i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <hr/>
                        <br/>
                        <table class="table table-responsive-sm table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tarikh Hantar</th>
                                    <th>Tarikh Terima</th>
                                    <th>No. DO</th>
                                    <th>No. Kod</th>
                                    <th>Jenis Aset</th>
                                    <th>Kuantiti Hantar</th>
                                    <th>Kuantiti Terima</th>
                                    <th>Baki</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <center>
                            <button class="btn btn-primary" type="submit">Simpan</button>
                            <button class="btn btn-primary" type="submit">Hantar</button>
                        </center>

                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
        @endsection
        <!-- /.conainer-fluid -->

