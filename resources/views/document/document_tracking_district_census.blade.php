@extends('master')
@section('content')

<div class="container-fluid">

    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-8">
<!--                 <div class="row">
                    <canvas id="document_statistic_by_state"  height="140"></canvas>
                    <button style="display: none" id="document_statistic_by_district_btn"  class="btn btn-primary" >Kembali</button>
                    <canvas style="display: none" id="document_statistic_by_district"  height="140"></canvas>
                </div> -->
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <label class="col-md-2 col-form-label" for="select3">Fasa Pembancian</label>
                            <div class="col-md-4">
                              <select class="form-control form-control-sm" id="select3" name="select3">
                                <option value="0">Pra Banci</option>
                                <option value="1">Option #1</option>
                                <option value="2">Option #2</option>
                                <option value="3">Option #3</option>
                              </select>
                            </div>
                        </div>
                        <br>
                        <!-- <div class="card">
                            <div class="card-body"> -->
                        <div class="row">
                            <label style="color:blue"><b>Negeri : 01 > &nbsp; Daerah Pentabiran : 01 >&nbsp;</b></label>
                            <label><b><u>Daerah Banci</u></b></label>
                        </div>
                        <div class="row">

                            <table class="table table-responsive-sm table-sm">
                              <thead>
                                <tr>
                                  <th>Kod</th>
                                  <th>Perancangan Agihan Kuantiti</th>
                                  <th>Diterima</th>
                                  <th>Baki Belum Terima</th>
                                  <th>Tahap</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>001</td>
                                  <td><center>1000</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>002</td>
                                  <td><center>400</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>003</td>
                                  <td><center>600</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>029</td>
                                  <td><center>300</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>031</td>
                                  <td><center>600</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>035</td>
                                  <td><center>800</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>047</td>
                                  <td><center>400</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>063</td>
                                  <td><center>700</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>064</td>
                                  <td><center>300</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>094</td>
                                  <td><center>2200</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                              </tbody>
                            </table>
                        <!-- </div> -->
                        </div>
                        <div class="row">
                            <label style="margin-left: 500px;">Maklumat</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-secondary" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;BELUM SELESAI</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-primary" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;JKK</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-warning" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;NEGERI</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-danger" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;PENGUASA DAERAH</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-dark" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;PENYELIA</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-success" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;PEMBANCI</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <label><u><center>Ringkasan status penerimaan dokumen peringkat daerah banci</center></u></label>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>001</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>002</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>003</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>029</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>031</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>035</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>047</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>063</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>064</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>094</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--/.row-->
</div>

</div>
@endsection
<!-- /.conainer-fluid -->

@section('myscript')
@endsection