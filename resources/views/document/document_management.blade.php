@extends('master')
@section('content')

<div class="container-fluid" style="background-color: white;">
    <div style="font-size: 24px;font-weight: bold;">Senarai Dokumen</div><br>
    <div>
        <a href="document_management/create" class="btn btn-primary active" role="button" aria-pressed="true" style="font-size: 10px;">(+)Tambah Senarai</a>
    </div><br>
    <table id='document-table' class="table table-responsive-sm">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
            </tr>
        </tfoot>
    </table>
</div>
@endsection
<!-- /.conainer-fluid -->

@section('myscript')
<script >
    $(document).ready(function () {
        $('#document-table').DataTable({
            serverSide: true,
            processing: true,
            responsive: true,
            ajax: "{{ route('document_list.index') }}",
            columns: [
                {"name" : 'id'},
                {"name" : 'name'},
                {"name" : 'email'},
            ],
        });
    });

</script>
@endsection


