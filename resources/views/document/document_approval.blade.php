@extends('master')
@section('content')

<div class="container-fluid">
          <div class="animated fadeIn">
            <div class="col-lg-4">
              <div class="row">
                 <div class="input-group">
                  <input class="form-control" id="input1-group2" type="text" name="input1-group2" placeholder="Cari Nama Dokumen">
                  <span class="input-group-prepend">
                    <button class="btn btn-primary" type="button">
                      <i class="fa fa-search"></i></button>
                  </span>
                </div>
              </div>
              <div class="col-lg-12">
                <div class="row">
                  <center>Maklumat Baki Dokumen Setiap Negeri</center>
                </div>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-lg-2">
                <div class="card">
                    <div class="card-body">JOHOR 0</div>
                </div>
                <div class="card">
                    <div class="card-body">KEDAH 0</div>
                </div>
                <div class="card">
                    <div class="card-body">KELANTAN 0</div>
                </div>
                <div class="card">
                    <div class="card-body">MELAKA 0</div>
                </div>
                <div class="card">
                    <div class="card-body">N.SEMBILAN 0</div>
                </div>
                <div class="card">
                    <div class="card-body">PAHANG 0</div>
                </div>
                <div class="card">
                    <div class="card-body">P.PINANG 0</div>
                </div>
                <div class="card">
                    <div class="card-body">PERAK 0</div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="card">
                    <div class="card-body">PERLIS 0</div>
                </div>
                <div class="card">
                    <div class="card-body">SELANGOR 0</div>
                </div>
                <div class="card">
                    <div class="card-body">TERENGGANU 0</div>
                </div>
                <div class="card">
                    <div class="card-body">SABAH 0</div>
                </div>
                <div class="card">
                    <div class="card-body">SARAWAK 0</div>
                </div>
                <div class="card">
                    <div class="card-body">KL 0</div>
                </div>
                <div class="card">
                    <div class="card-body">PUTRAJAYA 0</div>
                </div>
                <div class="card">
                    <div class="card-body">LABUAN 0</div>
                </div>
              </div>
              <!-- /.col-->
              <div class="col-lg-8">

                <div class="card">
                  <div class="card-header">
                    <i class="fa fa-align-justify"></i> Kelulusan Dokumen Bagi Negeri</div>
                  <div class="card-body">
                    <div class="form-group row">
                        &nbsp;&nbsp;&nbsp;
                        <!-- <button href="/document_request/create" class="col-md-2 btn btn-sm btn-primary" type="submit">Permintaan</button> -->
                        <a class="col-md-2 btn btn-sm btn-primary" href="document_request/create" role="button">Permintaan</a>
                      </div>
                    <table class="table table-responsive-sm table-sm">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Tarikh Permintaan</th>
                          <th>Kod Dokumen</th>
                          <th>Nama Dokumen</th>
                          <th>Negeri Pemohon</th>
                          <th>Negeri Dipohon</th>
                          <th>Kuantiti</th>
                          <th>Status</th>
                          <th>Tindakan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>30/08/2018</td>
                          <td>2</td>
                          <td>Soal Selidik</td>
                          <td>Johor</td>
                          <td>Melaka</td>
                          <td>100</td>
                          <td><button class="btn btn-sm btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Lulus</button></td>
                          <td>
                            <!-- <span class="badge badge-success">Active</span> -->
                            <button class="btn btn-sm btn-success" type="submit">
                      <i class="fa fa-dot-circle-o"></i></button>
                      <button class="btn btn-sm btn-danger" type="submit">
                      <i class="fa fa-dot-circle-o"></i></button>
                          </td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                    <ul class="pagination">
                      <li class="page-item">
                        <a class="page-link" href="#">Prev</a>
                      </li>
                      <li class="page-item active">
                        <a class="page-link" href="#">1</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">2</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">3</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">4</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <!-- /.col-->
            </div>



            <!-- Button trigger modal -->
<!--             <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
              Launch demo modal
            </button> -->

            <!-- Modal -->
            <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <center><h5 class="modal-title" id="exampleModalLabel">Kelulusan Permintaan Dokumen Bagi Negeri</h5></center>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-md-8">
                          <div class="col-md-8">
                              <label>Nama Dokumen</label>
                          </div>
                          <div class="col-md-50">
                              <input class="form-control" id="cvv" type="text" placeholder="Soal Selidik">
                          </div>
                          <br>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-8">
                          <div class="col-md-8">
                              <label>Kuantiti</label>
                          </div>
                          <div class="col-md-50">
                              <input class="form-control" id="cvv" type="text" placeholder="100">
                          </div>
                          <br>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-8">
                          <div class="col-md-8">
                              <label>Negeri Yang Meminta</label>
                          </div>
                          <div class="col-md-50">
                              <input class="form-control" id="cvv" type="text" placeholder="Johor">
                          </div>
                          <br>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-8">
                          <div class="col-md-8">
                              <label>Catatan</label>
                          </div>
                          <div class="col-md-50">
                              <input class="form-control" id="cvv" type="text" placeholder="Borang tidak mencukupi">
                          </div>
                          <br>
                        </div>
                      </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="card">
                      <div class="card-header bg-primary text-white">
                        <i class="fa fa-align-justify"></i> Diluluskan oleh
                      </div>
                      <div class="card-body">
                        <div class="row">
                          <div class="col-md-2">
                            <div class="row">
                              <label>Nama Pegawai</label>
                            </div>
                            <br>
                            <div class="row">
                              <label>Negeri</label>
                            </div>
                            <br>
                          </div>
                          <div class="col-md-4">
                            <div class="row">
                              <input class="form-control" id="cvv" type="text" placeholder="">
                            </div>
                            <br>
                            <div class="row">
                              <input class="form-control" id="cvv" type="text" placeholder="Melaka">
                            </div>
                            <br>
                          </div>
                          <div class="col-md-2">
                            <div class="row">
                              <label>Jawatan</label>
                            </div>
                            <br>
                            <div class="row">
                              <label>Tarikh</label>
                            </div>
                            <br>
                          </div>
                          <div class="col-md-4">
                            <div class="row">
                              <input class="form-control" id="cvv" type="text" placeholder="">
                            </div>
                            <br>
                            <div class="row">
                              <div class="input-group">
                                <input class="form-control form-control-sm" id="input1-group2" type="date" name="input1-group2" placeholder="Cari Nama Dokumen">
                                <span class="input-group-prepend">
                                  <button class="btn btn-secondary" type="button">
                                    <i class="fa fa-calendar"></i></button>
                                </span>
                              </div>
                            </div>
                            <br>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-2">
                            <div class="row">
                              <label>Catatan</label>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="row">
                              <input class="form-control" id="cvv" type="text" placeholder="">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer text-center">
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Terima</button>
                  <button type="button" class="btn btn-danger">Batal</button>
                </div>
              </div>
            </div>


@endsection
<!-- /.conainer-fluid -->

