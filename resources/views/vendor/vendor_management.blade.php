@extends('master')
@section('content')

<div class="container-fluid" style="background-color: white;">
    <div style="font-size: 24px;font-weight: bold;">Senarai Pembekal</div><br>
    <div class="col-lg-4">
        <div class="row">
            <div class="input-group">
              <input class="form-control" id="input1-group2" type="text" name="input1-group2" placeholder="Cari Nama Dokumen">
              <span class="input-group-prepend">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i></button>
              </span>
            </div>
        </div>
   </div>
   <br>
    <div>
        <a href="vendor_management/create" class="btn btn-primary active" role="button" aria-pressed="true" style="font-size: 10px;">(+)Tambah Senarai</a>
    </div><br>
    <table class="table table-responsive-sm">
        <thead>
            <tr style="background-color: #43B6D7;">
                <th>No </th>
                <th>Nama Pembekal</th>
                <th>Barang/Perkhidmatan yang dibekalkan</th>
                <th>No Telefon</th>
                <th>Status Pembekal</th>
                <th>Tindakan</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Percetakan Nasional Berhad</td>
                <td>Percetakan</td>
                <td>0172864590</td>
                <td>Aktif</td>
                <td>
                    <a href="document_management/edit" style="background-color: limegreen; border-color: limegreen;" class="btn btn-primary" role="button">
                        <i class="fa fa-pencil"></i>
                    </a>
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>Percetakan Suria Berhad</td>
                <td>Percetakan</td>
                <td>0133789100</td>
                <td>Aktif</td>
                <td>
                    <a href="document_management/edit" style="background-color: limegreen; border-color: limegreen;" class="btn btn-primary" role="button">
                        <i class="fa fa-pencil"></i>
                    </a>
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>Percetakan Harian</td>
                <td>Percetakan</td>
                <td>0127452001</td>
                <td>Aktif</td>
                <td>
                    <a href="document_management/edit" style="background-color: limegreen; border-color: limegreen;" class="btn btn-primary" role="button">
                        <i class="fa fa-pencil"></i>
                    </a>
                </td>
            </tr>
            <tr>
                <td>4</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>5</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>6</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>7</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>8</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>9</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <ul class="pagination">
        <li class="page-item">
            <a class="page-link" href="#">Sebelum</a>
        </li>
        <li class="page-item active">
            <a class="page-link" href="#">1</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">2</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">3</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">4</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">Seterusnya</a>
        </li>
    </ul>
    <br>
</div>
@endsection
<!-- /.conainer-fluid -->

