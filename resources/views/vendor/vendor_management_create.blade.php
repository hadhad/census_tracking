@extends('master')
@section('content')

<div class="container-fluid" style="background-color: white;">
    <div style="font-size: 24px;font-weight: bold;">Maklumat Pembekal</div><br>

    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">Nama Pembekal/Syarikat</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
        <label class="col-md-2 col-form-label" for="select3">Alamat pembekal</label>
        <div class="col-md-4">
            <textarea class="form-control" id="alamatpremis" name="textarea-input" rows="5" placeholder=""></textarea>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">No. Pendaftaran SSM</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">Email Syarikat</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
        <label class="col-md-2 col-form-label" for="select3">No. Telefon Syarikat</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">Website Pembekal</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
        <label class="col-md-2 col-form-label" for="select3">No. Fax</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="nolo">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">Status Pembekal</label>
        <div class="col-md-4">
            <select class="form-control form-control-sm" id="select3" name="select3">
                <option value="0">Sila pilih</option>
                <option value="1">Option #1</option>
                <option value="2">Option #2</option>
                <option value="3">Option #3</option>
            </select>
        </div>
    </div>
</div>
<div class="container-fluid" style="background-color: white;">
    <div style="font-size: 24px;font-weight: bold;">Maklumat Perhubungan Pembekal
    </div>
    <br>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">Nama</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
        <label class="col-md-2 col-form-label" for="select3">No. Tel</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">Jawatan</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
        <label class="col-md-2 col-form-label" for="select3">Email</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
    </div>
    <div>
        <br>
            <center>
                <button class="btn btn-success" type="submit">Kemaskini</button>
                <button class="btn btn-primary" type="submit">Hantar</button>
            </center>
        <br>
    </div>
</div>
<br>
<div class="container-fluid" style="background-color: white;">
    <div style="font-size: 24px;font-weight: bold;">Senarai Urusniaga</div><br>
    <div>
        <a href="../vendor_management_btype/create" class="btn btn-primary active" role="button" aria-pressed="true" style="font-size: 10px;">(+)Tambah Senarai</a>
    </div>
    <br>
    <table class="table table-responsive-sm">
        <thead>
            <tr style="background-color: #43B6D7;">
                <th>No </th>
                <th>No LO</th>
                <th>Tindakan</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td></td>
                <td>
                    <a href="document_management/edit" style="background-color: limegreen; border-color: limegreen;" class="btn btn-primary" role="button">
                        <i class="fa fa-pencil"></i>
                    </a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <ul class="pagination">
        <li class="page-item">
            <a class="page-link" href="#">Sebelum</a>
        </li>
        <li class="page-item active">
            <a class="page-link" href="#">1</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">2</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">3</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">4</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">Seterusnya</a>
        </li>
    </ul>
    <br>
</div>

@endsection
<!-- /.conainer-fluid -->

