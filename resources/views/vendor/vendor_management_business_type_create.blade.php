@extends('master')
@section('content')

<div class="container-fluid" style="background-color: white;">
    <div style="font-size: 24px;font-weight: bold;">Maklumat Urusniaga</div>
    <br>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">Jenis Urusniaga</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
        <label class="col-md-2 col-form-label" for="select3">Skop Urusniaga</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">Senarai Barang/Perkhidmatan Yang Dibekalkan</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
        <label class="col-md-2 col-form-label" for="select3">Tarikh Penghantaran Barang/ Perkhidmatan</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">No. Dokumen LO</label>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
        <div class="col-md-6">
            <label class="col-md-2 col-form-label" for="select3">Tempoh Pembekalan/Perkhidmatan/Kerja:</label>
            <div class="form-group row">
                <label class="col-md-2 col-form-label" for="select3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dari</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="kuantiti">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2 col-form-label" for="select3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hingga</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="kuantiti">
                </div>
            </div>
            <label class="col-md-2 col-form-label" for="select3">Tempoh Kontrak:</label>
            <div class="form-group row">
                <label class="col-md-2 col-form-label" for="select3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dari</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="kuantiti">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2 col-form-label" for="select3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hingga</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="kuantiti">
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">Senarai Anak Syarikat Yang Terlibat</label>
        <div class="col-md-1">1</div>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
        <div class="col-md-1">3</div>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3" style="visibility:hidden">Senarai Anak Syarikat Yang Terlibat</label>
        <div class="col-md-1">2</div>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
        <div class="col-md-1">4</div>
        <div class="col-md-4">
            <input type="text" class="form-control" id="kuantiti">
        </div>
    </div>
    <div>
        <br>
            <center>
                <button class="btn btn-success" type="submit">Kemaskini</button>
                <button class="btn btn-primary" type="submit">Hantar</button>
            </center>
        <br>
    </div>
</div>


@endsection
<!-- /.conainer-fluid -->

