@extends('master')
@section('content')

<div class="container-fluid">

    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-12">
              <div class="card">
                    <div class="card-body">
                        <div class="row">
                          <label>Nama Inventori : Soal Selidik</label>
                        </div>
                        <div class="row">
                          <table class="table table-responsive-sm table-striped">
                            <tbody>
                              <tr>
                                <td><img src="img/lori_masuk.png" alt="Smiley face" height="200" width="300"></td>
                                <td>
                                  <br>
                                  <br>
                                  <label style="color:blue">10 Ogos 2018, 12:38:37 PM</label><br><br>
                                  <label>Item diterima dari pembekal</label><br><br>
                                  <label>Stor Negeri Johor</label><br>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td><img src="img/lori_keluar.png" alt="Smiley face" height="200" width="300"></td>
                                <td>
                                  <br>
                                  <br>
                                  <label style="color:blue">10 Ogos 2018, 12:38:37 PM</label><br><br>
                                  <label>Item dihantar ke Daerah Pentadbiran</label><br><br>
                                  <label>Stor Negeri Johor</label><br>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td><img src="img/lori_belum_grey.png" alt="Smiley face" height="200" width="300"></td>
                                <td>
                                  <br>
                                  <br>
                                  <label style="color:blue">10 Ogos 2018, 12:38:37 PM</label><br><br>
                                  <label>Dalam proses menghantar</label><br><br>
                                  <label>Stor Negeri Johor</label><br>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td><img src="img/lori_belum_white.png" alt="Smiley face" height="200" width="300"></td>
                                <td>
                                  <br>
                                  <br>
                                  <label style="color:blue">10 Ogos 2018, 12:38:37 PM</label><br><br>
                                  <label>Dalam proses menghantar</label><br><br>
                                  <label>Stor Negeri Johor</label><br>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td><img src="img/lori_belum_grey.png" alt="Smiley face" height="200" width="300"></td>
                                <td>
                                  <br>
                                  <br>
                                  <label style="color:blue">10 Ogos 2018, 12:38:37 PM</label><br><br>
                                  <label>Dalam proses menghantar</label><br><br>
                                  <label>Stor Negeri Johor</label><br>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td><img src="img/lori_belum_white.png" alt="Smiley face" height="200" width="300"></td>
                                <td>
                                  <br>
                                  <br>
                                  <label style="color:blue">10 Ogos 2018, 12:38:37 PM</label><br><br>
                                  <label>Dalam proses menghantar</label><br><br>
                                  <label>Stor Negeri Johor</label><br>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                    </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
<!-- /.conainer-fluid -->

@section('myscript')
@endsection