@extends('master')
@section('content')

<div class="container-fluid">

    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-8">
                <div class="row">
                    <canvas id="document_statistic_by_state"  height="140"></canvas>
                    <button style="display: none" id="document_statistic_by_district_btn"  class="btn btn-primary" >Kembali</button>
                    <canvas style="display: none" id="document_statistic_by_district"  height="140"></canvas>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie1" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie2" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie3" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie4" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie5" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie6" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie7" ></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card bg-light">
                            <div class="card-body pb-0">
                                <canvas height="400" id="pie8" ></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <canvas id="inventory_statistic_by_state" height="140"></canvas>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="text-center card-body pb-0">
                                <h3>10,000,000</h3>
                                Dokumen
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="text-center card-body pb-0">
                                <h3>5,000</h3>
                                Aset
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="text-center card-body pb-0">
                                <h3>10,000</h3>
                                Inventori
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="text-center card-body pb-0">
                                <h3>3,500</h3>
                                Personel
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="text-center card-body pb-0">
                                <h3>90</h3>
                                Vendor
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="text-center card-body pb-0">
                                <h3>50</h3>
                                Premis
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <canvas id="personel_stats" height="400"></canvas>
                </div>
            </div>
            <!--/.col-->
        </div>
        <!--/.row-->
    </div>

</div>
@endsection
<!-- /.conainer-fluid -->

@section('myscript')
<script src="{{ asset('js/views/dashboard_dokumen_statistic.js') }}"></script>
<script src="{{ asset('js/views/dashboard_inventory_statistic.js') }}"></script>
<script src="{{ asset('js/views/dashboard_piechart.js') }}"></script>
<script src="{{ asset('js/views/dashboard_personel.js') }}"></script>
@endsection