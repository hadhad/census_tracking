<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                <a href="{{ route('dashboard.index') }}">CENSUS TRACKING</a>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-doc"></i> DOKUMEN</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            Pengurusan Dokumen</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('document_management.index') }}">
                                     Senarai Dokumen</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('document_planning.index') }}">
                                     Perancangan Dokumen</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('document_request.index') }}">
                                     Permintaan Dokumen</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link " href="{{ route('document_approval.index') }}">
                            Kelulusan Dokumen</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="{{ route('document_store_record.create') }}">
                            Pengurusan Rekod Stor</a>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            Penghantaran Dokumen</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('document_delivery_pre_census.create') }}">
                                     Pra/Semasa Banci</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('document_delivery_post_census.create') }}">
                                     Pasca Banci</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            Penerimaan Dokumen</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('document_acceptance_pre_census.create') }}">
                                     Pra/Semasa Banci</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('document_acceptance_post_census.create') }}">
                                     Pasca Banci</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link " href="{{ route('document_tracking.index') }}">
                            Tracking Dokumen</a>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link " href="{{ route('document_management.index') }}">
                            Pelupusan Dokumen</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-doc"></i> ASET</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            Pengurusan Aset</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('asset_management.index') }}">
                                     Senarai Aset</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('asset_planning.index') }}">
                                     Perancangan Aset</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('asset_request.index') }}">
                                     Permintaan Aset</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link " href="{{ route('asset_approval.index') }}">
                            Kelulusan Aset</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="{{ route('asset_store_record.create') }}">
                            Pengurusan Rekod Stor</a>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            Penghantaran Aset</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('asset_delivery_pre_census.create') }}">
                                     Pra/Semasa Banci</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('asset_delivery_post_census.create') }}">
                                     Pasca Banci</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            Penerimaan Aset</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('asset_acceptance_pre_census.create') }}">
                                     Pra/Semasa Banci</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('asset_acceptance_post_census.create') }}">
                                     Pasca Banci</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link " href="{{ route('asset_tracking.index') }}">
                            Tracking Aset</a>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link " href="{{ route('asset_management.index') }}">
                            Pelupusan Aset</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-doc"></i> INVENTORI</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            Pengurusan Inventori</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('inventory_management.index') }}">
                                     Senarai Inventori</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('inventory_planning.index') }}">
                                     Perancangan Inventori</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('inventory_request.index') }}">
                                     Permintaan Inventori</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link " href="{{ route('inventory_approval.index') }}">
                            Kelulusan Inventori</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="{{ route('inventory_store_record.create') }}">
                            Pengurusan Rekod Stor</a>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            Penghantaran Inventori</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('inventory_delivery_pre_census.create') }}">
                                     Pra/Semasa Banci</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('inventory_delivery_post_census.create') }}">
                                     Pasca Banci</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            Penerimaan Inventori</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('inventory_acceptance_pre_census.create') }}">
                                     Pra/Semasa Banci</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('inventory_acceptance_post_census.create') }}">
                                     Pasca Banci</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link " href="{{ route('inventory_tracking.index') }}">
                            Tracking Inventori</a>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link " href="{{ route('inventory_management.index') }}">
                            Pelupusan Inventori</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-user"></i> PERSONEL</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link " href="{{ route('personnel_dashboard.index') }}">
                            Dashboard</a>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            Pengurusan Personel</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('personnel.index') }}">
                                     Senarai Personel</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('personnel_recruitment.index') }}">
                                     Pengambilan Personel</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link " href="{{ route('replacement.index') }}">
                            Penggantian/Penamatan</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-grid"></i> VENDOR</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('vendor_management.index') }}">
                            Pengurusan Vendor</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-home"></i> PREMIS</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="">
                            Pengurusan Premis</a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
