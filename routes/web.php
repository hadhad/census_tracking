<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Auth::routes();
/* CoreUI templates */

Route::middleware('auth')->group(function ()
{
    Route::resource('/', 'DashboardController');
    Route::resource('dashboard', 'DashboardController');
    Route::resource('document_management', 'Document\DocumentManagementController');
    Route::resource('document_planning', 'Document\DocumentPlanningController');
    Route::resource('document_request', 'Document\DocumentRequestController');
    Route::resource('document_request_district', 'Document\DocumentRequestDistrictController');
    Route::resource('document_approval', 'Document\DocumentApprovalController');
    Route::resource('document_approval_district', 'Document\DocumentApprovalDistrictController');
    Route::resource('document_delivery_pre_census', 'Document\DocumentDeliveryPreCensusController');
    Route::resource('document_delivery_post_census', 'Document\DocumentDeliveryPostCensusController');
    Route::resource('document_acceptance_pre_census', 'Document\DocumentAcceptancePreCensusController');
    Route::resource('document_acceptance_post_census', 'Document\DocumentAcceptancePostCensusController');
    Route::resource('document_store_record', 'Document\DocumentStoreRecordController');
    Route::resource('document_tracking', 'Document\DocumentTrackingController');
    Route::resource('document_tracking_state', 'Document\DocumentTrackingStateController');
    Route::resource('document_tracking_district_admin', 'Document\DocumentTrackingDistrictAdminController');
    Route::resource('document_tracking_district', 'Document\DocumentTrackingDistrictController');
    Route::resource('document_tracking_eb', 'Document\DocumentTrackingEBController');
    Route::resource('document_tracking_acceptance', 'Document\DocumentTrackingAcceptanceController');



    Route::resource('vendor_dashboard', 'Vendor\VendorDashboardController');
    Route::resource('vendor_list', 'Vendor\VendorListController');
    Route::resource('vendor_management', 'Vendor\VendorManagementController');
    Route::resource('vendor_management_btype', 'Vendor\VendorManagementBTypeController');

    Route::resource('asset_management', 'Asset\AssetManagementController');
    Route::resource('asset_planning', 'Asset\AssetPlanningController');
    Route::resource('asset_request', 'Asset\AssetRequestController');
    Route::resource('asset_request_district', 'Asset\AssetRequestDistrictController');
    Route::resource('asset_approval', 'Asset\AssetApprovalController');
    Route::resource('asset_approval_district', 'Asset\AssetApprovalDistrictController');
    Route::resource('asset_delivery_pre_census', 'Asset\AssetDeliveryPreCensusController');
    Route::resource('asset_delivery_post_census', 'Asset\AssetDeliveryPostCensusController');
    Route::resource('asset_acceptance_pre_census', 'Asset\AssetAcceptancePreCensusController');
    Route::resource('asset_acceptance_post_census', 'Asset\AssetAcceptancePostCensusController');
    Route::resource('asset_store_record', 'Asset\AssetStoreRecordController');
    Route::resource('asset_tracking', 'Asset\AssetTrackingController');
    Route::resource('asset_tracking_state', 'Asset\AssetTrackingStateController');
    Route::resource('asset_tracking_district_admin', 'Asset\AssetTrackingDistrictAdminController');
    Route::resource('asset_tracking_district', 'Asset\AssetTrackingDistrictController');
    Route::resource('asset_tracking_eb', 'Asset\AssetTrackingEBController');
    Route::resource('asset_tracking_acceptance', 'Asset\AssetTrackingAcceptanceController');

    Route::resource('inventory_management', 'Inventory\InventoryManagementController');
    Route::resource('inventory_planning', 'Inventory\InventoryPlanningController');
    Route::resource('inventory_request', 'Inventory\InventoryRequestController');
    Route::resource('inventory_request_district', 'Inventory\InventoryRequestDistrictController');
    Route::resource('inventory_approval', 'Inventory\InventoryApprovalController');
    Route::resource('inventory_approval_district', 'Inventory\InventoryApprovalDistrictController');
    Route::resource('inventory_delivery_pre_census', 'Inventory\InventoryDeliveryPreCensusController');
    Route::resource('inventory_delivery_post_census', 'Inventory\InventoryDeliveryPostCensusController');
    Route::resource('inventory_acceptance_pre_census', 'Inventory\InventoryAcceptancePreCensusController');
    Route::resource('inventory_acceptance_post_census', 'Inventory\InventoryAcceptancePostCensusController');
    Route::resource('inventory_store_record', 'Inventory\InventoryStoreRecordController');
    Route::resource('inventory_tracking', 'Inventory\InventoryTrackingController');
    Route::resource('inventory_tracking_state', 'Inventory\InventoryTrackingStateController');
    Route::resource('inventory_tracking_admin', 'Inventory\InventoryTrackingDistrictAdminController');
    Route::resource('inventory_tracking_district', 'Inventory\InventoryTrackingDistrictController');
    Route::resource('inventory_tracking_eb', 'Inventory\InventoryTrackingEBController');
    Route::resource('inventory_tracking_acceptance', 'Inventory\InventoryTrackingAcceptanceController');

    Route::resource('personnel_dashboard', 'Personnel\PersonnelDashboardController');
    Route::resource('personnel', 'Personnel\PersonnelController');
    Route::resource('personnel_recruitment', 'Personnel\PersonnelRecruitmentListController');
    Route::resource('recruitment_interview_invitation', 'Personnel\PersonnelRecruitmentInterviewInvitationController');
    Route::resource('recruitment_interview_result', 'Personnel\PersonnelRecruitmentInterviewResultController');
    Route::resource('recruitment_acceptance', 'Personnel\PersonnelRecruitmentAcceptanceController');
    Route::resource('recruitment_info', 'Personnel\PersonnelRecruitmentInfoController');
    Route::resource('replacement', 'Personnel\PersonnelReplacementController');
});
// Section Pages
Route::view('/sample/error404', 'errors.404')->name('error404');
Route::view('/sample/error500', 'errors.500')->name('error500');
